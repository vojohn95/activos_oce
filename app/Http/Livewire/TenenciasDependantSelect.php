<?php

namespace App\Http\Livewire;

use Livewire\Component;

class TenenciasDependantSelect extends Component
{
    public $estatusTenencia, $comentarios, $value = 0;

    public function render()
    {
        return view('livewire.tenencias-dependant-select');
    }

    public function updatedEstatusTenencia($estatusTenencia)
    {
        if($estatusTenencia == "Pendientes"){
            $this->value = 1;
        }else{
            $this->value = 0;
            $this->reset('comentarios');
        }

    }


}
