<?php

namespace App\Http\Livewire;

use Livewire\Component;

class VerificacionesDependantSelect extends Component
{
    public function render()
    {
        return view('livewire.verificaciones-dependant-select');
    }
}
