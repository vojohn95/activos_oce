<?php

namespace App\Http\Livewire;

use Livewire\Component;

class TenenciasDependantSelectedit extends Component
{
    public $estatusTenencia, $comentarios, $value = 0, $estatus;

    public function mount($estatus)
    {
        $this->estatusTenencia = $estatus;
    }

    public function render()
    {
        return view('livewire.tenencias-dependant-selectedit');
    }

    public function updatedEstatusTenencia($estatusTenencia)
    {
        if ($estatusTenencia == "Pendientes") {
            $this->value = 1;
        } else {
            $this->value = 0;
            $this->reset('comentarios');
        }
    }
}
