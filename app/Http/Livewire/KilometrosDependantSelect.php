<?php

namespace App\Http\Livewire;

use App\Models\Auto;
use Livewire\Component;

class KilometrosDependantSelect extends Component
{

    public $idAuto = null, $kilometraje, $idAutoEdit;

    public function mount($idAutoEdit = null)
    {
        $this->idAutoEdit = $idAutoEdit;
        if(!is_null($this->idAutoEdit)){
            $this->idAuto = $this->idAutoEdit;
            $this->kilometraje = Auto::find($this->idAutoEdit)->kmFinal;
        }
    }
    public function render()
    {
        $autos = Auto::pluck('placas', 'id');

        return view('livewire.kilometros-dependant-select', compact('autos'));
    }

    public function updatedIdAuto($idAuto)
    {
        $this->kilometraje = Auto::select('kmFinal')->find($idAuto)->kmFinal;
    }

}
