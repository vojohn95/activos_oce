<!-- Id Marca Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id_marca', 'Marca:') !!}   
    {!! Form::select('id_marca', $marcas, $modeloEquipo->modelo ?? '', ['class' => 'form-control', 'required']); !!}
</div>

<!-- Modelo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('modelo', 'Modelo:') !!}
    {!! Form::text('modelo', null, ['class' => 'form-control','maxlength' => 255,'maxlength' => 255]) !!}
</div>

<!-- Anio Field -->
{{-- <div class="form-group col-sm-6">
    {!! Form::label('anio', 'Año:') !!}
    {!! Form::number('anio', null, ['class' => 'form-control']) !!}
</div> --}}

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Enviar', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('modeloEquipos.index') }}" class="btn btn-light">Cancelar</a>
</div>
