<!-- Id Tipoequipo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id_tipoEquipo', 'Tipo equipo:') !!}
    {!! Form::select('id_tipoEquipo', $tipoequipos, $marcaEquipo->id_tipoEquipo ?? '', ['class' => 'form-control', 'required']); !!}
</div>


<!-- Marca Field -->
<div class="form-group col-sm-6">
    {!! Form::label('marca', 'Marca:') !!}
    {!! Form::text('marca', null, ['class' => 'form-control','maxlength' => 100,'maxlength' => 100]) !!}
</div>

<!-- Fabricante Field -->
<div class="form-group col-sm-6">
    {!! Form::label('fabricante', 'Fabricante:') !!}
    {!! Form::text('fabricante', null, ['class' => 'form-control','maxlength' => 100,'maxlength' => 100]) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Enviar', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('marcaEquipos.index') }}" class="btn btn-light">Cancelar</a>
</div>
