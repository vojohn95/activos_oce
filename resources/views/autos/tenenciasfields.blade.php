<!-- Idtenencias Field -->
<div class="form-group col-sm-6">
    {!! Form::label('idTenencias', 'ID Tenencias:') !!}
    {!! Form::number('idTenencias', $tenencias->idTenencias ?? '', ['class' => 'form-control']) !!}
</div>

<!-- Anio Field -->
<div class="form-group col-sm-6">
    {!! Form::label('anio', 'Año:') !!}
    {!! Form::number('anio', $tenencias->anio ?? '', ['class' => 'form-control']) !!}
</div>


@if ($estatus != 0)
    @livewire('tenencias-dependant-selectedit', ['estatus' => $tenencias->estatus])
@else
    @livewire('tenencias-dependant-select')
@endif
