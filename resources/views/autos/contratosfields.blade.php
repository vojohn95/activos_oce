<!-- Id Arrendadora Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id_arrendadora', 'Arrendadora:') !!}
    {!! Form::select('id_arrendadora', $arrendadoras, $contrato->id_arrendadora ?? '', [
        'class' => 'form-control',
    ]) !!}
</div>
@php
if (isset($contratos->plazo) != null) {
    $porciones = explode(' ', $contratos->plazo);
}
@endphp
<!-- Plazo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('plazo', 'Plazo:') !!}
    {!! Form::select(
        'plazo',
        [
            'Sin Plazo' => 'Sin Plazo',
            '6 Meses' => '6 Meses',
            '12 Meses' => '12 Meses',
            '24 Meses' => '24 Meses',
            '36 Meses' => '36 Meses',
            '48 Meses' => '48 Meses',
        ],
        $porciones ?? 'Sin Plazo',
        ['class' => 'form-control'],
    ) !!}
</div>

<!-- Fecha Inicio Field -->
<div class="form-group col-sm-6">
    {!! Form::label('fecha_inicio', 'Fecha Inicio:') !!}
    {!! Form::date('fecha_inicio', $contratos->fecha_inicio ?? '', [
        'class' => 'form-control',
        'id' => 'fecha_inicio',
    ]) !!}
</div>

@push('scripts')
    <script type="text/javascript">
        $('#fecha_inicio').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: true,
            sideBySide: true,
        })
    </script>
@endpush

<!-- Fecha Final Field -->
<div class="form-group col-sm-6">
    {!! Form::label('fecha_final', 'Fecha Final:') !!}
    {!! Form::date('fecha_final', $contratos->fecha_final ?? '', ['class' => 'form-control', 'id' => 'fecha_final']) !!}
</div>

@push('scripts')
    <script type="text/javascript">
        $('#fecha_final').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: true,
            sideBySide: true
        })
    </script>
@endpush

<!-- Monto Inicial Field -->
<div class="form-group col-sm-6">
    {!! Form::label('monto_inicial', 'Monto Inicial:') !!}
    {!! Form::number('monto_inicial', $contratos->monto_inicial ?? '', ['class' => 'form-control']) !!}
</div>

<!-- Monto Mensualidad Field -->
<div class="form-group col-sm-6">
    {!! Form::label('monto_mensualidad', 'Monto Mensualidad:') !!}
    {!! Form::number('monto_mensualidad', $contratos->monto_mensualidad ?? '', ['class' => 'form-control']) !!}
</div>
