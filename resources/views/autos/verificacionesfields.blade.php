<!-- Idtenencias Field -->
<div class="form-group col-sm-6">
    {!! Form::label('estatusVerificacion', 'Estatus:') !!}
    {!! Form::select('estatusVerificacion', [null => 'Seleccione una opción..', 'Al corriente' => 'Al corriente', 'Pendiente' => 'Pendiente'],$verificaciones->estatus ?? '', ['class' => 'form-control', 'required', 'placeholder'=>'Por favor seleccione ...']); !!}
</div>

<!-- Anio Field -->
<div class="form-group col-sm-6">
    {!! Form::label('siguiente_verificacion', 'Siguiente verificación:') !!}
    <input type="month" id="siguiente_verificacion" name="siguiente_verificacion" min="2018-03" value="{{$verificaciones->siguiente_verificacion ?? '2022-05'}}" class="form-control">
</div>
