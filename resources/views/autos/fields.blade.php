<!-- Idtipo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('idTipo', 'Tipo:') !!}
    {!! Form::select('idTipo', $tipos, $auto->idTipo ?? '', ['class' => 'form-control']) !!}
</div>

<!-- Idmarca Field -->
<div class="form-group col-sm-6">
    {!! Form::label('idMarca', 'Marca:') !!}
    {!! Form::select('idMarca', $marcas, $auto->idMarca ?? '', ['class' => 'form-control']) !!}
</div>

<!-- Idmodelo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('idmodelo', 'Modelo:') !!}
    {!! Form::select('idmodelo', $modelos, $auto->idmodelo ?? '', ['class' => 'form-control']) !!}
</div>

<!-- Idempleado Field -->
<div class="form-group col-sm-6">
    {!! Form::label('idEmpleado', 'Empleado:') !!}
    {!! Form::select('idEmpleado', $empleados, $auto->idEmpleado ?? '', ['class' => 'form-control']) !!}
</div>

<!-- Foto Field -->
<div class="form-group col-sm-6 col-lg-6">
    {!! Form::label('foto', 'Foto:') !!}
    <input type="file" class="form-control-file" id="foto" name="foto">

</div>

<div class="form-group col-sm-6">
    {!! Form::label('no_poliza', 'Número póliza:') !!}
    {!! Form::text('no_poliza', $auto->no_poliza ?? '', ['class' => 'form-control', 'maxlength' => 45]) !!}
</div>


<!-- Color Field -->
<div class="form-group col-sm-6">
    {!! Form::label('color', 'Color:') !!}
    {!! Form::text('color', $auto->color ?? '', ['class' => 'form-control', 'maxlength' => 45, 'maxlength' => 45]) !!}
</div>

<!-- Placas Field -->
<div class="form-group col-sm-6">
    {!! Form::label('numeroSerie', 'Número Serie:') !!}
    {!! Form::text('numeroSerie', $auto->numeroSerie ?? '', [
        'class' => 'form-control',
        'maxlength' => 45,
        'maxlength' => 45,
    ]) !!}
</div>

<!-- Placas Field -->
<div class="form-group col-sm-6">
    {!! Form::label('numeroMotor', 'Número Motor:') !!}
    {!! Form::text('numeroMotor', $auto->numeroMotor ?? '', [
        'class' => 'form-control',
        'maxlength' => 45,
        'maxlength' => 45,
    ]) !!}
</div>

<!-- Placas Field -->
<div class="form-group col-sm-6">
    {!! Form::label('placas', 'Placas:') !!}
    {!! Form::text('placas', $auto->placas ?? '', ['class' => 'form-control', 'maxlength' => 45, 'maxlength' => 45]) !!}
</div>

<!-- Anio Field -->
<div class="form-group col-sm-6">
    {!! Form::label('anio', 'Año:') !!}
    {!! Form::number('anio', $auto->anio ?? '', ['class' => 'form-control']) !!}
</div>

<!-- Precio Compra Field -->
<div class="form-group col-sm-6">
    {!! Form::label('precio_Compra', 'Precio Compra:') !!}
    {!! Form::number('precio_Compra', $auto->precio_Compra ?? '', ['class' => 'form-control']) !!}
</div>

<!-- Kminicial Field -->
<div class="form-group col-sm-6">
    {!! Form::label('kmInicial', 'Km inicial:') !!}
    {!! Form::number('kmInicial', null, ['class' => 'form-control']) !!}
</div>

<!-- Kmfinal Field -->
<div class="form-group col-sm-6">
    {!! Form::label('kmFinal', 'Km final:') !!}
    {!! Form::number('kmFinal', null, ['class' => 'form-control']) !!}
</div>

<!-- Estatus Field -->
<div class="form-group col-sm-12">
    {!! Form::label('estatus', 'Estatus:') !!}
    <select class="form-control" id="estatus" name="estatus">
        <option value=" Activo">Activo</option>
        <option value="Inactivo">Inactivo</option>
    </select>
</div>
