<div>
    <!-- Idauto Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('idAuto', 'Auto:') !!}
        {!! Form::select('idAuto', $autos, $mantenimientoAuto->idAuto ?? '', ['class' => 'form-control', 'required', 'wire:model' => 'idAuto','placeholder'=>'Por favor seleccione ...']); !!}
    </div>

    <!-- Kilometraje Field -->
    <div class="form-group col-sm-12">
        <div wire:loading>
            Cargando...
            <div class="spinner-border text-primary" role="status">
                <span class="sr-only">Loading...</span>
              </div>
        </div>
        <div wire:loading.remove>
        {!! Form::label('kilometraje', 'Kilometraje:') !!}
        {!! Form::number('kilometraje', null, ['class' => 'form-control', 'required', 'wire:model' => 'kilometraje']) !!}
        </div>
    </div>
</div>
<br>
