<div>
    <div class="form-group col-sm-12">
        {!! Form::label('estatusTenencia', 'Estatus:') !!}
        {{-- {!! Form::select('estatusTenencia', [null => 'Seleccione un estatus'] +['Al corriente' => 'Al corriente','Pendientes'=>'Pendientes'], ['wire:model' => 'estatusTenencia' ,'class' => 'form-control', 'required' ,'placeholder'=>'Por favor seleccione ...']); !!} --}}
        <select wire:model="estatusTenencia" name="estatusTenencia" class="form-control">
            <option value="" selected>Seleccione un estatus</option>
            <option value="Al corriente">Al corriente</option>
            <option value="Pendientes">Pendientes</option>
        </select>
    </div>
    @if ($value)
    <div class="form-group col-sm-12">
        <div wire:loading>
            Cargando...
            <div class="spinner-border text-primary" role="status">
                <span class="sr-only">Loading...</span>
              </div>
        </div>
        <div wire:loading.remove>
        {!! Form::label('comentarios', 'Comentarios:') !!}
        {!! Form::textarea('comentarios', null, ['class' => 'form-control', 'required', 'wire:model.defer' => 'comentarios']) !!}
        </div>
    </div>
    @endif

</div>
